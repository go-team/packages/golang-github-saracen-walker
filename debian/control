Source: golang-github-saracen-walker
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Jai Flack <jflack@disroot.org>
Section: devel
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-golang-x-sync-dev
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-saracen-walker
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-saracen-walker.git
Homepage: https://github.com/saracen/walker
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/saracen/walker

Package: golang-github-saracen-walker-dev
Architecture: all
Depends: golang-golang-x-sync-dev,
         ${misc:Depends}
Description: walker is a faster, parallel version, of filepath.Walk
 This library and filepath.Walk both perform os.Lstat calls and provide a full
 os.FileInfo structure to the callback. BenchmarkFastwalkWalkLstat and
 BenchmarkGodirwalkWalkLstat include this stat call for better comparison with
 BenchmarkFilepathWalk and BenchmarkWalkerWalk.
 .
 This library and fastwalk both require the callback to be safe for concurrent
 use. BenchmarkFilepathWalkAppend, BenchmarkWalkerWalkAppend,
 BenchmarkFastwalkWalkAppend and BenchmarkGodirwalkWalkAppend append the paths
 found to a string slice. The callback, for the libraries that require it, use a
 mutex, for better comparison with the libraries that require no locking.
 .
 This library will not always be the best/fastest option. In general, if you're
 on Windows, or performing lstat calls, it does a pretty decent job. If you're
 not, I've found fastwalk to perform better on machines with fewer cores.
